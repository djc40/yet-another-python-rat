- figure out how the custom import function works
    - so can do some basic stuff using just exec and ignoring the custom import stuff
    - not sure what problems I may run into as things get more advanced
    - might start with this and then move to the custom import if I run into problems
    - initial thought is to maintain a list or dict of functions. Then when trying to call if its not there it gets downloaded / exec'd and added to the list/dict
    - think this should work with just some basic exec type func. Not sure if it will work with nested remote imports but i'll do some tests on that later
- basic client server
    - socket class looks like no timeout by default even if you wait like 5 mins between commands, kinda cool
- basic encryption
    - aes time (objectively bad idea here we come)
- ssh like message format for multiplexing traffic over one connection
- remotely loadable modules
- first module loaded and results transferred back


# how does aes work
- symmetric algorithm
- works on blocks of 16 bytes (128 bits)
- 14 rounds for 256

# pseudocode
function AESencrypt(plaintext, key) {
  blocks := divideIntoBlocks(plaintext);
  roundKeys = getRoundKeys(key)  for (block in blocks) {    //first round
    addRoundKey(roundKeys[0], block);    //intermediate rounds
    for (8, 10 or 12 rounds) {
      subBytes(block);
      shiftRows(block);
      mixColumns(block);
      addRoundKey(roundKeys[..], block);
    }    //last round
    subBytes(block);
    shiftRows(block);
    addRoundKey(roundKeys[numRounds - 1], block);  }  ciphertext := reassemble(blocks);
  return ciphertext;}


