import basic_server
import aes
import os
import threading

def bytes_to_int(bytes_obj):
    result = 0
    for b in bytes_obj:
        result = result * 256 + int(b)
    return result

key = bytes.fromhex('5a215b82583b90ae15fb03403893981311111111111111112222222222222222')

encryptor = aes.AES(key)
setup = input()
socket_type, ip, port = setup.split(",")
buffer = bytearray()
testClient = basic_server.TCPClient(socket_type, ip, int(port), buffer)
mutex = threading.Lock()
received = bytes()
while True:
    message = input()
    if message == "p":
        with mutex:
            received += bytes(buffer)
            buffer.clear()
        if received:
            # test if received contains a full message
            #print(received.hex())
            length = bytes_to_int(received[:2])
            #print(length)
            if len(received) < (length + 2):
                continue
            iv = received[2:18]
            ciphertext = received[18:(length+2)]
            plaintext = encryptor.decrypt_cbc(ciphertext,iv)
            #print(iv.hex())
            print(plaintext)
            received = received[(length+2):]
    elif message == "quit":
        exit
    else:
        plaintext = bytes(message, 'utf-8')
        while len(plaintext) > 65500:
            current_message = plaintext[:65000]
            plaintext = plaintext[65000:]
            iv = os.urandom(16)
            ciphertext = encryptor.encrypt_cbc(current_message,iv)
            length = format(len(iv) + len(ciphertext),'04x')
            length_header = bytes.fromhex(length)
            testClient.sendMessage(length_header+iv+ciphertext)

        iv = os.urandom(16)
        ciphertext = encryptor.encrypt_cbc(plaintext,iv)
        length = format(len(iv) + len(ciphertext),'04x')
        length_header = bytes.fromhex(length)
        testClient.sendMessage(length_header+iv+ciphertext)
