import unittest
import aes

class TestBasicMethods(unittest.TestCase):

    def test_sbox_lookup(self):
        self.assertEqual(aes.sbox_lookup(0x01),0x7c)
        self.assertEqual(aes.sbox_lookup(10),0x67)
        self.assertEqual(aes.sbox_lookup(0xab),0x62)

    def test_i_sbox_lookup(self):
        self.assertEqual(aes.i_sbox_lookup(0x01),0x09)
        self.assertEqual(aes.i_sbox_lookup(10),0xa3)
        self.assertEqual(aes.i_sbox_lookup(0xab),0x0e)

    def test_rot_word(self):
        self.assertEqual(aes.rot_word(b'\x01\x02\x03\x04'),b'\x02\x03\x04\x01')
        self.assertEqual(aes.rot_word(bytes.fromhex('01020304'),2),b'\x03\x04\x01\x02')
        self.assertEqual(aes.rot_word(b'\x01\x02\x03\x04',3),b'\x04\x01\x02\x03')

    def test_shift_rows(self):
        state = aes.create_state(bytes.fromhex('d42711aee0bf98f1b8b45de51e415230'))
        state_after_shift = aes.create_state(bytes.fromhex('d4bf5d30e0b452aeb84111f11e2798e5'))
        aes.shift_rows(state)
        self.assertEqual(state,state_after_shift)

    def test_sub_word(self):
        self.assertEqual(aes.sub_word(b'\x14\x83\x72\x9a'),b'\xfa\xec\x40\xb8')

    def test_sub_bytes(self):
        start = [bytes.fromhex('193de3be'), bytes.fromhex('a0f4e22b'),bytes.fromhex('9ac68d2a'),bytes.fromhex('e9f84808')]
        end = [bytes.fromhex('d42711ae'),bytes.fromhex('e0bf98f1'),bytes.fromhex('b8b45de5'),bytes.fromhex('1e415230')]
        aes.sub_bytes(start)
        self.assertEqual(start,end)

    def test_bytes_xor(self):
        self.assertEqual(aes.bytes_xor(b'\x14\x83\x72\x9a',b'\xab\x37\x90\xaf'),b'\xbf\xb4\xe2\x35')
        self.assertEqual(aes.bytes_xor(b'\x14\x83\x72\x9a',bytes.fromhex('ab3790af')),b'\xbf\xb4\xe2\x35')
        self.assertEqual(aes.bytes_xor(b'\x00\x00\x00\x00',bytes.fromhex('ffffffff')),bytes.fromhex('ffffffff'))

    def test_mix_column(self):
        self.assertEqual(aes.mix_column(bytes.fromhex('d4bf5d30')),bytes.fromhex('046681e5'))
        self.assertEqual(aes.mix_column(bytes.fromhex('e0b452ae')),bytes.fromhex('e0cb199a'))
        self.assertEqual(aes.mix_column(bytes.fromhex('b84111f1')),bytes.fromhex('48f8d37a'))
        self.assertEqual(aes.mix_column(bytes.fromhex('1e2798e5')),bytes.fromhex('2806264c'))

    def test_mix_columns(self):
        state = aes.create_state(bytes.fromhex('d4bf5d30e0b452aeb84111f11e2798e5'))
        state_after_mix = aes.create_state(bytes.fromhex('046681e5e0cb199a48f8d37a2806264c'))
        aes.mix_columns(state)
        self.assertEqual(state,state_after_mix)
    
    def test_add_round_key(self):
        pass

    def test_create_state(self):
        a = bytes.fromhex('000102030405060708090a0b0c0d0e0f')
        b = [b'\x00\x01\x02\x03',b'\x04\x05\x06\x07',b'\x08\x09\x0a\x0b',b'\x0c\x0d\x0e\x0f']
        self.assertEqual(aes.create_state(a),b)

    def test_expand_key128(self):
        K = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')
        list_of_expanded_keys = [bytes.fromhex('2b7e1516'),
                                bytes.fromhex('28aed2a6'),
                                bytes.fromhex('abf71588'),
                                bytes.fromhex('09cf4f3c'),
                                bytes.fromhex('a0fafe17'),
                                bytes.fromhex('88542cb1'),
                                bytes.fromhex('23a33939'),
                                bytes.fromhex('2a6c7605'),
                                bytes.fromhex('f2c295f2'),
                                bytes.fromhex('7a96b943'),
                                bytes.fromhex('5935807a'),
                                bytes.fromhex('7359f67f'),
                                bytes.fromhex('3d80477d'),
                                bytes.fromhex('4716fe3e'),
                                bytes.fromhex('1e237e44'),
                                bytes.fromhex('6d7a883b'),
                                bytes.fromhex('ef44a541'),
                                bytes.fromhex('a8525b7f'),
                                bytes.fromhex('b671253b'),
                                bytes.fromhex('db0bad00'),
                                bytes.fromhex('d4d1c6f8'),
                                bytes.fromhex('7c839d87'),
                                bytes.fromhex('caf2b8bc'),
                                bytes.fromhex('11f915bc'),
                                bytes.fromhex('6d88a37a'),
                                bytes.fromhex('110b3efd'),
                                bytes.fromhex('dbf98641'),
                                bytes.fromhex('ca0093fd'),
                                bytes.fromhex('4e54f70e'),
                                bytes.fromhex('5f5fc9f3'),
                                bytes.fromhex('84a64fb2'),
                                bytes.fromhex('4ea6dc4f'),
                                bytes.fromhex('ead27321'),
                                bytes.fromhex('b58dbad2'),
                                bytes.fromhex('312bf560'),
                                bytes.fromhex('7f8d292f'),
                                bytes.fromhex('ac7766f3'),
                                bytes.fromhex('19fadc21'),
                                bytes.fromhex('28d12941'),
                                bytes.fromhex('575c006e'),
                                bytes.fromhex('d014f9a8'),
                                bytes.fromhex('c9ee2589'),
                                bytes.fromhex('e13f0cc8'),
                                bytes.fromhex('b6630ca6')]
        self.assertEqual(aes.AES(K).expand_key(K),list_of_expanded_keys)


    def test_expand_key192(self):
        K = bytes.fromhex('8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b')
        list_of_expanded_keys = [bytes.fromhex('8e73b0f7'),
                                bytes.fromhex('da0e6452'),
                                bytes.fromhex('c810f32b'),
                                bytes.fromhex('809079e5'),
                                bytes.fromhex('62f8ead2'),
                                bytes.fromhex('522c6b7b'),
                                bytes.fromhex('fe0c91f7'),
                                bytes.fromhex('2402f5a5'),
                                bytes.fromhex('ec12068e'),
                                bytes.fromhex('6c827f6b'),
                                bytes.fromhex('0e7a95b9'),
                                bytes.fromhex('5c56fec2'),
                                bytes.fromhex('4db7b4bd'),
                                bytes.fromhex('69b54118'),
                                bytes.fromhex('85a74796'),
                                bytes.fromhex('e92538fd'),
                                bytes.fromhex('e75fad44'),
                                bytes.fromhex('bb095386'),
                                bytes.fromhex('485af057'),
                                bytes.fromhex('21efb14f'),
                                bytes.fromhex('a448f6d9'),
                                bytes.fromhex('4d6dce24'),
                                bytes.fromhex('aa326360'),
                                bytes.fromhex('113b30e6'),
                                bytes.fromhex('a25e7ed5'),
                                bytes.fromhex('83b1cf9a'),
                                bytes.fromhex('27f93943'),
                                bytes.fromhex('6a94f767'),
                                bytes.fromhex('c0a69407'),
                                bytes.fromhex('d19da4e1'),
                                bytes.fromhex('ec1786eb'),
                                bytes.fromhex('6fa64971'),
                                bytes.fromhex('485f7032'),
                                bytes.fromhex('22cb8755'),
                                bytes.fromhex('e26d1352'),
                                bytes.fromhex('33f0b7b3'),
                                bytes.fromhex('40beeb28'),
                                bytes.fromhex('2f18a259'),
                                bytes.fromhex('6747d26b'),
                                bytes.fromhex('458c553e'),
                                bytes.fromhex('a7e1466c'),
                                bytes.fromhex('9411f1df'),
                                bytes.fromhex('821f750a'),
                                bytes.fromhex('ad07d753'),
                                bytes.fromhex('ca400538'),
                                bytes.fromhex('8fcc5006'),
                                bytes.fromhex('282d166a'),
                                bytes.fromhex('bc3ce7b5'),
                                bytes.fromhex('e98ba06f'),
                                bytes.fromhex('448c773c'),
                                bytes.fromhex('8ecc7204'),
                                bytes.fromhex('01002202')]
        self.assertEqual(aes.AES(K).expand_key(K),list_of_expanded_keys)

    def test_expand_key256(self):
        K = bytes.fromhex('603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4')
        list_of_expanded_keys = [bytes.fromhex('603deb10'),
                                bytes.fromhex('15ca71be'),
                                bytes.fromhex('2b73aef0'),
                                bytes.fromhex('857d7781'),
                                bytes.fromhex('1f352c07'),
                                bytes.fromhex('3b6108d7'),
                                bytes.fromhex('2d9810a3'),
                                bytes.fromhex('0914dff4'),
                                bytes.fromhex('9ba35411'),
                                bytes.fromhex('8e6925af'),
                                bytes.fromhex('a51a8b5f'),
                                bytes.fromhex('2067fcde'),
                                bytes.fromhex('a8b09c1a'),
                                bytes.fromhex('93d194cd'),
                                bytes.fromhex('be49846e'),
                                bytes.fromhex('b75d5b9a'),
                                bytes.fromhex('d59aecb8'),
                                bytes.fromhex('5bf3c917'),
                                bytes.fromhex('fee94248'),
                                bytes.fromhex('de8ebe96'),
                                bytes.fromhex('b5a9328a'),
                                bytes.fromhex('2678a647'),
                                bytes.fromhex('98312229'),
                                bytes.fromhex('2f6c79b3'),
                                bytes.fromhex('812c81ad'),
                                bytes.fromhex('dadf48ba'),
                                bytes.fromhex('24360af2'),
                                bytes.fromhex('fab8b464'),
                                bytes.fromhex('98c5bfc9'),
                                bytes.fromhex('bebd198e'),
                                bytes.fromhex('268c3ba7'),
                                bytes.fromhex('09e04214'),
                                bytes.fromhex('68007bac'),
                                bytes.fromhex('b2df3316'),
                                bytes.fromhex('96e939e4'),
                                bytes.fromhex('6c518d80'),
                                bytes.fromhex('c814e204'),
                                bytes.fromhex('76a9fb8a'),
                                bytes.fromhex('5025c02d'),
                                bytes.fromhex('59c58239'),
                                bytes.fromhex('de136967'),
                                bytes.fromhex('6ccc5a71'),
                                bytes.fromhex('fa256395'),
                                bytes.fromhex('9674ee15'),
                                bytes.fromhex('5886ca5d'),
                                bytes.fromhex('2e2f31d7'),
                                bytes.fromhex('7e0af1fa'),
                                bytes.fromhex('27cf73c3'),
                                bytes.fromhex('749c47ab'),
                                bytes.fromhex('18501dda'),
                                bytes.fromhex('e2757e4f'),
                                bytes.fromhex('7401905a'),
                                bytes.fromhex('cafaaae3'),
                                bytes.fromhex('e4d59b34'),
                                bytes.fromhex('9adf6ace'),
                                bytes.fromhex('bd10190d'),
                                bytes.fromhex('fe4890d1'),
                                bytes.fromhex('e6188d0b'),
                                bytes.fromhex('046df344'),
                                bytes.fromhex('706c631e')]
        self.assertEqual(aes.AES(K).expand_key(K),list_of_expanded_keys)

    def test_full_round_test(self):
        K = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')
        W = aes.AES(K).expand_key(K)
        state = aes.create_state(bytes.fromhex('3243f6a8885a308d313198a2e0370734'))
        after_round_key = aes.create_state(bytes.fromhex('193de3bea0f4e22b9ac68d2ae9f84808'))
        after_sub_bytes = aes.create_state(bytes.fromhex('d42711aee0bf98f1b8b45de51e415230'))
        after_shift_rows = aes.create_state(bytes.fromhex('d4bf5d30e0b452aeb84111f11e2798e5'))
        after_mix_columns = aes.create_state(bytes.fromhex('046681e5e0cb199a48f8d37a2806264c'))

        aes.add_round_key(state,W[0:4])
        self.assertEqual(state,after_round_key)
        aes.sub_bytes(state)
        self.assertEqual(state,after_sub_bytes)
        aes.shift_rows(state)
        self.assertEqual(state,after_shift_rows)
        aes.mix_columns(state)
        self.assertEqual(state,after_mix_columns)

    def test_cipher(self):
        plaintext = bytes.fromhex('00112233445566778899aabbccddeeff')
        key_128 = bytes.fromhex('000102030405060708090a0b0c0d0e0f')
        ciphertext_128 = bytes.fromhex('69c4e0d86a7b0430d8cdb78070b4c55a')
        self.assertEqual(aes.AES(key_128).cipher(plaintext),ciphertext_128)

        key_192 = bytes.fromhex('000102030405060708090a0b0c0d0e0f1011121314151617')
        ciphertext_192 = bytes.fromhex('dda97ca4864cdfe06eaf70a0ec0d7191')
        self.assertEqual(aes.AES(key_192).cipher(plaintext),ciphertext_192)

        key_256 = bytes.fromhex('000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f')
        ciphertext_256 = bytes.fromhex('8ea2b7ca516745bfeafc49904b496089')
        self.assertEqual(aes.AES(key_256).cipher(plaintext),ciphertext_256)

    def test_inv_cipher(self):
        plaintext = bytes.fromhex('00112233445566778899aabbccddeeff')
        key_128 = bytes.fromhex('000102030405060708090a0b0c0d0e0f')
        ciphertext_128 = bytes.fromhex('69c4e0d86a7b0430d8cdb78070b4c55a')
        self.assertEqual(aes.AES(key_128).inv_cipher(ciphertext_128),plaintext)

        key_192 = bytes.fromhex('000102030405060708090a0b0c0d0e0f1011121314151617')
        ciphertext_192 = bytes.fromhex('dda97ca4864cdfe06eaf70a0ec0d7191')
        self.assertEqual(aes.AES(key_192).inv_cipher(ciphertext_192),plaintext)

        key_256 = bytes.fromhex('000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f')
        ciphertext_256 = bytes.fromhex('8ea2b7ca516745bfeafc49904b496089')
        self.assertEqual(aes.AES(key_256).inv_cipher(ciphertext_256),plaintext)

    def test_inv_shift_rows(self):
        start = aes.create_state(bytes.fromhex('7ad5fda789ef4e272bca100b3d9ff59f'))
        end = aes.create_state(bytes.fromhex('7a9f102789d5f50b2beffd9f3dca4ea7'))
        aes.inv_shift_rows(start)
        self.assertEqual(start,end)

    def test_inv_sub_bytes(self):
        start = aes.create_state(bytes.fromhex('7a9f102789d5f50b2beffd9f3dca4ea7'))
        end = aes.create_state(bytes.fromhex('bd6e7c3df2b5779e0b61216e8b10b689'))
        aes.inv_sub_bytes(start)
        self.assertEqual(start,end)

    def test_inv_sub_word(self):
        self.assertEqual(aes.inv_sub_word(b'\x7a\x9f\x10\x27'),b'\xbd\x6e\x7c\x3d')

    def test_inv_mix_columns(self):
        start = aes.create_state(bytes.fromhex('e9f74eec023020f61bf2ccf2353c21c7'))
        end = aes.create_state(bytes.fromhex('54d990a16ba09ab596bbf40ea111702f'))
        aes.inv_mix_columns(start)
        self.assertEqual(start,end)

    def test_pad(self):
        plaintext1 = b'hello'
        padded = aes.pad(plaintext1)
        self.assertEqual(padded,b'hello\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b')

        plaintext2 = b'abcdefghijklmnop'
        padded = aes.pad(plaintext2)
        self.assertEqual(padded,b'abcdefghijklmnop\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10')

        plaintext3 = b'abcdefghijklmno'
        padded = aes.pad(plaintext3)
        self.assertEqual(padded,b'abcdefghijklmno\x01')

    def test_unpad(self):
        plaintext1 = b'hello'
        padded = b'hello\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b'
        self.assertEqual(aes.unpad(padded),plaintext1)

        plaintext2 = b'abcdefghijklmnop'
        padded = b'abcdefghijklmnop\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10'
        self.assertEqual(aes.unpad(padded),plaintext2)

        plaintext3 = b'abcdefghijklmno'
        padded = b'abcdefghijklmno\x01'
        self.assertEqual(aes.unpad(padded),plaintext3)

    def test_encrypt_cbc(self):
        K = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')
        iv = bytes.fromhex('000102030405060708090A0B0C0D0E0F')
        plaintext = bytes.fromhex('6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710')
        ciphertext = bytes.fromhex('7649abac8119b246cee98e9b12e9197d5086cb9b507219ee95db113a917678b273bed6b8e3c1743b7116e69e222295163ff1caa1681fac09120eca307586e1a78cb82807230e1321d3fae00d18cc2012')
        self.assertEqual(aes.AES(K).encrypt_cbc(plaintext,iv),ciphertext)

    def test_decrypt_cbc(self):
        K = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')
        iv = bytes.fromhex('000102030405060708090A0B0C0D0E0F')
        plaintext = bytes.fromhex('6bc1bee22e409f96e93d7e117393172aae2d8a571e03ac9c9eb76fac45af8e5130c81c46a35ce411e5fbc1191a0a52eff69f2445df4f9b17ad2b417be66c3710')
        ciphertext = bytes.fromhex('7649abac8119b246cee98e9b12e9197d5086cb9b507219ee95db113a917678b273bed6b8e3c1743b7116e69e222295163ff1caa1681fac09120eca307586e1a78cb82807230e1321d3fae00d18cc2012')
        self.assertEqual(aes.AES(K).decrypt_cbc(ciphertext,iv),plaintext)




if __name__ == '__main__':
    unittest.main()
