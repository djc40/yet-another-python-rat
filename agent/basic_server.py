import socket
import threading

class TCPClient:
    def __init__(self, server_type, ip, port, buffer):
        self.server_type = server_type
        self.ip = ip
        self.port = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn = None
        self.addr = None
        self.mutex = threading.Lock()
        if self.server_type == "listen":
            self.conn, self.addr = self.listen(self.s, self.ip, self.port)
            threading.Thread(target=self.readData, args=(buffer,)).start()
        elif self.server_type == "connect":
            self.connect(self.s, self.ip, self.port)
            self.conn = self.s
            threading.Thread(target=self.readData, args=(buffer,)).start()

    def listen(self, s, ip, port):
        print('--listen called--')
        s.bind((ip, port))
        s.listen()
        conn, addr = s.accept()
        return conn, addr

    def connect(self, s, ip, port):
        print('--connect called--')
        s.connect((ip, port))

    def sendMessage(self, message):
        print('--send message called--')
        self.conn.sendall(message)

    def readData(self, buffer):
        print('--read data called--')
        while True:
            data = self.conn.recv(1024)
            if data:
                with self.mutex:
                    buffer += data
