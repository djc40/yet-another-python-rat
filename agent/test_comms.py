#W!/usr/bin/env python3
import os
import threading

Nb = 4 # num of 32 bit words in block

s_box = [[0x63,0x7c,0x77,0x7b,0xf2,0x6b,0x6f,0xc5,0x30,0x01,0x67,0x2b,0xfe,0xd7,0xab,0x76],
        [0xca,0x82,0xc9,0x7d,0xfa,0x59,0x47,0xf0,0xad,0xd4,0xa2,0xaf,0x9c,0xa4,0x72,0xc0],
        [0xb7,0xfd,0x93,0x26,0x36,0x3f,0xf7,0xcc,0x34,0xa5,0xe5,0xf1,0x71,0xd8,0x31,0x15],
        [0x04,0xc7,0x23,0xc3,0x18,0x96,0x05,0x9a,0x07,0x12,0x80,0xe2,0xeb,0x27,0xb2,0x75],
        [0x09,0x83,0x2c,0x1a,0x1b,0x6e,0x5a,0xa0,0x52,0x3b,0xd6,0xb3,0x29,0xe3,0x2f,0x84],
        [0x53,0xd1,0x00,0xed,0x20,0xfc,0xb1,0x5b,0x6a,0xcb,0xbe,0x39,0x4a,0x4c,0x58,0xcf],
        [0xd0,0xef,0xaa,0xfb,0x43,0x4d,0x33,0x85,0x45,0xf9,0x02,0x7f,0x50,0x3c,0x9f,0xa8],
        [0x51,0xa3,0x40,0x8f,0x92,0x9d,0x38,0xf5,0xbc,0xb6,0xda,0x21,0x10,0xff,0xf3,0xd2],
        [0xcd,0x0c,0x13,0xec,0x5f,0x97,0x44,0x17,0xc4,0xa7,0x7e,0x3d,0x64,0x5d,0x19,0x73],
        [0x60,0x81,0x4f,0xdc,0x22,0x2a,0x90,0x88,0x46,0xee,0xb8,0x14,0xde,0x5e,0x0b,0xdb],
        [0xe0,0x32,0x3a,0x0a,0x49,0x06,0x24,0x5c,0xc2,0xd3,0xac,0x62,0x91,0x95,0xe4,0x79],
        [0xe7,0xc8,0x37,0x6d,0x8d,0xd5,0x4e,0xa9,0x6c,0x56,0xf4,0xea,0x65,0x7a,0xae,0x08],
        [0xba,0x78,0x25,0x2e,0x1c,0xa6,0xb4,0xc6,0xe8,0xdd,0x74,0x1f,0x4b,0xbd,0x8b,0x8a],
        [0x70,0x3e,0xb5,0x66,0x48,0x03,0xf6,0x0e,0x61,0x35,0x57,0xb9,0x86,0xc1,0x1d,0x9e],
        [0xe1,0xf8,0x98,0x11,0x69,0xd9,0x8e,0x94,0x9b,0x1e,0x87,0xe9,0xce,0x55,0x28,0xdf],
        [0x8c,0xa1,0x89,0x0d,0xbf,0xe6,0x42,0x68,0x41,0x99,0x2d,0x0f,0xb0,0x54,0xbb,0x16]]

i_s_box = [[0x52,0x09,0x6a,0xd5,0x30,0x36,0xa5,0x38,0xbf,0x40,0xa3,0x9e,0x81,0xf3,0xd7,0xfb],
        [0x7c,0xe3,0x39,0x82,0x9b,0x2f,0xff,0x87,0x34,0x8e,0x43,0x44,0xc4,0xde,0xe9,0xcb],
        [0x54,0x7b,0x94,0x32,0xa6,0xc2,0x23,0x3d,0xee,0x4c,0x95,0x0b,0x42,0xfa,0xc3,0x4e],
        [0x08,0x2e,0xa1,0x66,0x28,0xd9,0x24,0xb2,0x76,0x5b,0xa2,0x49,0x6d,0x8b,0xd1,0x25],
        [0x72,0xf8,0xf6,0x64,0x86,0x68,0x98,0x16,0xd4,0xa4,0x5c,0xcc,0x5d,0x65,0xb6,0x92],
        [0x6c,0x70,0x48,0x50,0xfd,0xed,0xb9,0xda,0x5e,0x15,0x46,0x57,0xa7,0x8d,0x9d,0x84],
        [0x90,0xd8,0xab,0x00,0x8c,0xbc,0xd3,0x0a,0xf7,0xe4,0x58,0x05,0xb8,0xb3,0x45,0x06],
        [0xd0,0x2c,0x1e,0x8f,0xca,0x3f,0x0f,0x02,0xc1,0xaf,0xbd,0x03,0x01,0x13,0x8a,0x6b],
        [0x3a,0x91,0x11,0x41,0x4f,0x67,0xdc,0xea,0x97,0xf2,0xcf,0xce,0xf0,0xb4,0xe6,0x73],
        [0x96,0xac,0x74,0x22,0xe7,0xad,0x35,0x85,0xe2,0xf9,0x37,0xe8,0x1c,0x75,0xdf,0x6e],
        [0x47,0xf1,0x1a,0x71,0x1d,0x29,0xc5,0x89,0x6f,0xb7,0x62,0x0e,0xaa,0x18,0xbe,0x1b],
        [0xfc,0x56,0x3e,0x4b,0xc6,0xd2,0x79,0x20,0x9a,0xdb,0xc0,0xfe,0x78,0xcd,0x5a,0xf4],
        [0x1f,0xdd,0xa8,0x33,0x88,0x07,0xc7,0x31,0xb1,0x12,0x10,0x59,0x27,0x80,0xec,0x5f],
        [0x60,0x51,0x7f,0xa9,0x19,0xb5,0x4a,0x0d,0x2d,0xe5,0x7a,0x9f,0x93,0xc9,0x9c,0xef],
        [0xa0,0xe0,0x3b,0x4d,0xae,0x2a,0xf5,0xb0,0xc8,0xeb,0xbb,0x3c,0x83,0x53,0x99,0x61],
        [0x17,0x2b,0x04,0x7e,0xba,0x77,0xd6,0x26,0xe1,0x69,0x14,0x63,0x55,0x21,0x0c,0x7d]]

def create_state(block):
    """
    create_state takes a 128 bit block of input and transforms it into the 2d array of bytes
    called the state (rows and columns are going to be opposite of the rows / columns of the standard)
    create_state() = 

    :param block: 16 byte input
    :return: a list of 4 bytes objects like so: [bytes, bytes, bytes, bytes]
    """


    # state is actually going to be an array of four bytes objects
    # shift rows is the only part where that has to change and can do the [bytes(x) of x in zip(*state)] and then switch back
    state = [ [] for i in range(Nb) ]

    for i in range(Nb):
        state[i] = block[i*4:(i+1)*4]

    return state

def state_to_bytes(state):
    """
    state_to_bytes takes in a state object and then returns the list of bytes

    :param state: state object which is a list of 4 bytes objects
    :return: a 16 byte long bytes object
    """

    return state[0] + state[1] + state[2] + state[3]
        



def print_state(state):
    """
    print_state is a helper method for printing the values of the state
    """
    for i in state:
        print(i.hex())
    print()



def sbox_lookup(byte):
    """
    sbox_lookup performs the s-box lookup for a byte using the higher nible as the row
    and the lower nible as the column.
    sbox_lookup(0xab) = 0x62

    :param byte: a single byte (0-255) that is used for the lookup. i.e. 0x01 (must be an int not a bytes obj because >> does
    not work on bytes object in python
    :return: the corresponding value in the s-box table
    """
    x = byte >> 4
    y = byte & 0x0f
    return s_box[x][y]

def i_sbox_lookup(byte):
    """
    i_sbox_lookup performs the inverse s-box lookup for a byte using the higher nible as the row
    and the lower nible as the column.
    i_sbox_lookup(0xab) = 0x0e

    :param byte: a single byte (0-255) that is used for the lookup. i.e. 0x01 (must be an int not a bytes obj because >> does
    not work on bytes object in python
    :return: the corresponding value in the inverse s-box table
    """
    x = byte >> 4
    y = byte & 0x0f
    return i_s_box[x][y]

def rot_word(word,n=1):
    """
    rot_word performs a one-byte left circular shift n times.
    rot_word(b'\x01\x02\x03\x04) = b'\x02\x03\x04\x01'
    rot_word(bytes.fromhex('01020304'),2) = b'\x03\x04\x01\x02'

    :param word: a list with 4 elements
    :return: a list with 4 elements that has been left circular shifted
    """
    return word[n:] + word[0:n]

def shift_rows(state):
    """
    shift_rows performs the shiftrows transformation on the state
    the first row is not shifted and then each row after is shifted one more than the previous.

    :param state: a list of 4 4-bytes objects
    """

    # transpose state so that rows are actually rows
    new_state = [bytes(x) for x in zip(*state)]
    new_state[1] = rot_word(new_state[1])
    new_state[2] = rot_word(new_state[2],2)
    new_state[3] = rot_word(new_state[3],3)

    # transpose back
    new_state = [bytes(x) for x in zip(*new_state)]

    state[0] = new_state[0]
    state[1] = new_state[1]
    state[2] = new_state[2]
    state[3] = new_state[3]

def inv_shift_rows(state):
    """
    inv_shift_rows performs the inverse shiftrows transformation on the state

    :param state: a list of 4 4-bytes objects
    """

    # transpose state so that rows are actually rows
    new_state = [bytes(x) for x in zip(*state)]
    new_state[1] = rot_word(new_state[1],3)
    new_state[2] = rot_word(new_state[2],2)
    new_state[3] = rot_word(new_state[3],1)

    # transpose back
    new_state = [bytes(x) for x in zip(*new_state)]

    state[0] = new_state[0]
    state[1] = new_state[1]
    state[2] = new_state[2]
    state[3] = new_state[3]


def sub_word(word):
    """
    sub_word performs the AES s-box lookup on each of the four bytes of the word
    sub_word(b'\x01\x02\x03\x04') = b'\x7c\x77\x7b\xf2'

    :param word: a bytes object of length 4
    :return: a bytes object of length 4 that has had the sbox lookup applied to each byte
    """
    b0 = sbox_lookup(word[0])
    b1 = sbox_lookup(word[1])
    b2 = sbox_lookup(word[2])
    b3 = sbox_lookup(word[3])
    return bytes([b0,b1,b2,b3])

def sub_bytes(state):
    """
    sub_bytes performs the AES sub_bytes step on the state "object"

    :param state: a list of 4 4-bytes objects
    """

    state[0] = sub_word(state[0])
    state[1] = sub_word(state[1])
    state[2] = sub_word(state[2])
    state[3] = sub_word(state[3])

def inv_sub_word(word):
    """
    inv_sub_word performs the AES inverse s-box lookup on each of the four bytes of the word
    sub_word(b'\x01\x02\x03\x04') = b'\x09\x6a\xd5\x30'

    :param word: a bytes object of length 4
    :return: a bytes object of length 4 that has had the sbox lookup applied to each byte
    """
    b0 = i_sbox_lookup(word[0])
    b1 = i_sbox_lookup(word[1])
    b2 = i_sbox_lookup(word[2])
    b3 = i_sbox_lookup(word[3])
    return bytes([b0,b1,b2,b3])

def inv_sub_bytes(state):
    """
    inv_sub_bytes performs the AES inv_sub_bytes step on the state "object"

    :param state: a list of 4 4-bytes objects
    """
    state[0] = inv_sub_word(state[0])
    state[1] = inv_sub_word(state[1])
    state[2] = inv_sub_word(state[2])
    state[3] = inv_sub_word(state[3])

def bytes_xor(word1, word2):
    """
    bytes_xor perfoms a bitwise xor for two bytes objects
    bytes_xor(b'\x14\x83\x72\x9a',b'\xab\x37\x90\xaf').hex() = 'bfb4e235'

    :param word1: a bytes object
    :param word2: a bytes object
    :return: a bytes object
    """
    return bytes(i^j for i,j in zip(word1,word2))

def mix_column(word):
    """
    mix_column performs the mix_column step of aes on a single column
    mix_column(b'\xdb\x13\x53\x45') = b'\x8e\x4d\xa1\xbc'

    :param word: a bytes object of length 4 representing a single column
    :return: a bytes object of length 4 which is the resulting column
    """

    b = bytearray(4)

    for i,j in enumerate(word):
        h = j >> 7
        b[i] = (j << 1) & 0xff
        b[i] ^= h * 0x1b

    r0 = b[0] ^ word[3] ^ word[2] ^ b[1] ^ word[1]
    r1 = b[1] ^ word[0] ^ word[3] ^ b[2] ^ word[2]
    r2 = b[2] ^ word[1] ^ word[0] ^ b[3] ^ word[3]
    r3 = b[3] ^ word[2] ^ word[1] ^ b[0] ^ word[0]

    return bytes([r0,r1,r2,r3])

def xtime(x):
    if (x & 0x80) == 0x80:
        return x << 1 & 0xff ^ 0x1b
    else:
        return x << 1

def mix_column2(word):
    a,b,c,d = word
    e = a ^ b ^ c ^ d
    r0 = a ^ e ^ xtime(a^b)
    r1 = b ^ e ^ xtime(b^c)
    r2 = c ^ e ^ xtime(c^d)
    r3 = d ^ e ^ xtime(d^a)
    return bytes([r0,r1,r2,r3])

def inv_mix_column(word):
    """
    inv_mix_column performs the .mix_column step of aes on a single column
    inv_mix_column(b'') = b''

    :param word: a bytes object of length 4 representing a single column
    :return: a bytes object of length 4 which is the resulting column
    """

    a,b,c,d = word
    r0 = xtime(xtime(xtime(a)^a)^a) ^ xtime(xtime(xtime(b))^b)^b ^ xtime(xtime(xtime(c)^c))^c ^ xtime(xtime(xtime(d)))^d
    r1 = xtime(xtime(xtime(a)))^a ^ xtime(xtime(xtime(b)^b)^b) ^ xtime(xtime(xtime(c))^c)^c ^ xtime(xtime(xtime(d)^d))^d
    r2 = xtime(xtime(xtime(a)^a))^a ^ xtime(xtime(xtime(b)))^b ^ xtime(xtime(xtime(c)^c)^c) ^ xtime(xtime(xtime(d))^d)^d
    r3 = xtime(xtime(xtime(a))^a)^a ^ xtime(xtime(xtime(b)^b))^b ^ xtime(xtime(xtime(c)))^c ^ xtime(xtime(xtime(d)^d)^d)

    return bytes([r0,r1,r2,r3])

def inv_mix_column2(word):
    return mix_column2(mix_column2(mix_column2(word)))

def mix_columns(state):
    """
    mix_columns performs the AES mixcolumns step on the state "object"

    :param state: a list of 4 4-bytes objects
    """

    state[0] = mix_column2(state[0])
    state[1] = mix_column2(state[1])
    state[2] = mix_column2(state[2])
    state[3] = mix_column2(state[3])

def inv_mix_columns(state):
    """
    inv_mix_columns performs the AES inv_mixcolumns step on the state

    :param state: a list of 4 4-bytes objects
    """

    state[0] = inv_mix_column(state[0])
    state[1] = inv_mix_column(state[1])
    state[2] = inv_mix_column(state[2])
    state[3] = inv_mix_column(state[3])

def add_round_key(state, round_key):
    """
    add_round_key performs the AES add_round_key step on the state "object"

    :param state:
    """

    state[0] = bytes_xor(state[0],round_key[0])
    state[1] = bytes_xor(state[1],round_key[1])
    state[2] = bytes_xor(state[2],round_key[2])
    state[3] = bytes_xor(state[3],round_key[3])

def pad(plaintext):
    """
    pad pads the given plaintext with PKCS#7 padding

    :param plaintext: the input plaintext (bytes) that will eventually be encrypted
    :return: the plaintext (bytes) concatenated with the padding
    """

    padding_len = 16 - (len(plaintext) % 16)
    padding = bytes([padding_len] * padding_len)
    return plaintext + padding

def unpad(padded):
    """
    unpad removes PKCS#7 padding

    :param padded: the input bytes object that contains padding
    :return: the original unpadded plaintext
    """

    padding_len = padded[-1]
    plaintext = padded[:-padding_len]
    return plaintext

def gen_iv():
    return os.urandom(16)

class AES:
    def __init__(self, key):
        self.K = key
        self.Nk = len(key)//4 # num of 32 bit words in cipher key K
        self.Nr = max(self.Nk,Nb) + 7 # num of round keys needed (11 for AES-128, 13 for AES-192, 15 for AES-256)
        self.W = self.expand_key(self.K)


    def cipher(self,block):
        """
        cipher does the actual encrypting of a 128 bit section of input

        :param block: 16 byte input
        :return: cipher text
        """

        state = create_state(block)

        add_round_key(state,self.W[0:4])

        for i in range(self.Nr-2):
            sub_bytes(state)
            shift_rows(state)
            mix_columns(state)
            add_round_key(state,self.W[(i+1)*4:(i+2)*4])

        sub_bytes(state)
        shift_rows(state)
        add_round_key(state,self.W[-4:])

        return state_to_bytes(state)
        

    def inv_cipher(self,block):
        state = create_state(block)

        add_round_key(state,self.W[-4:])

        for i in range(self.Nr-2)[::-1]:
            inv_shift_rows(state)
            inv_sub_bytes(state)
            add_round_key(state,self.W[(i+1)*4:(i+2)*4])
            inv_mix_columns(state)

        inv_shift_rows(state)
        inv_sub_bytes(state)
        add_round_key(state,self.W[0:4])

        return state_to_bytes(state)

    def expand_key(self,K):
        """
        expand_key performs aes key expansion to turn the cipher key into separate round keys

        :param K: the cipher key in bytes form
        :return: 
        """

        W = [None] * (self.Nr * 4)

        # calculate round constants
        rcon = [bytes([0x01, 0x00, 0x00, 0x00])]

        for _ in range(1, self.Nr-1):
            rc_prev = rcon[-1][0]

            if rc_prev < 0x80:
                rc_i = 2 * rc_prev
            else:
                rc_i = 2 * rc_prev ^ 0x11b

            rcon.append(bytes([rc_i,0,0,0]))

        # break the key into Nk 32bit words
        for i in range(self.Nk):
            W[i] = K[i*4:(i*4)+4]

        # get the rest of the keys
        for i in range(self.Nk, self.Nr*Nb):
            temp = W[i-1]
            if i % self.Nk == 0:
                temp = bytes_xor(sub_word(rot_word(temp)), rcon[i//self.Nk - 1])
            elif self.Nk > 6 and i % self.Nk == 4:
                temp = sub_word(temp)

            W[i] = bytes_xor(W[i-self.Nk],temp)

        return W

    def encrypt_cbc(self,plaintext, iv):
        """
        encrypt_cbc encrypts plaintext using CBC mode where each block is xor'd by the previous before being encrypted

        :param plaintext: a bytes object of the plaintext to be encrypted
        :param iv: bytes object of len 16
        :return: encrypted cipher text
        """
        plaintext = pad(plaintext)
        previous = iv
        result = bytes()
        for i in range(0, len(plaintext), 16):
            block = plaintext[i:i+16]
            encrypted = self.cipher(bytes_xor(block,previous))
            result += encrypted
            previous = encrypted
        return result

    def decrypt_cbc(self,ciphertext, iv):
        """
        decrypt_cbc decrypts ciphertext using CBC mode where each block is xor'd by the previous before being encrypted

        :param ciphertext: a bytes object that has already been encrypted using encrypt_cbc
        :param iv: bytes object of len 16
        :return: decrypted plain text
        """
        previous = iv
        result = bytes()
        for i in range(0, len(ciphertext), 16):
            block = ciphertext[i:i+16]
            decrypted = bytes_xor(previous, self.inv_cipher(block))
            result += decrypted
            previous = block
        return unpad(result)
        
#!/usr/bin/env python3

import socket
import threading

class TCPClient:
    def __init__(self, server_type, ip, port, buffer):
        self.server_type = server_type
        self.ip = ip
        self.port = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.conn = None
        self.addr = None
        self.mutex = threading.Lock()
        if self.server_type == "listen":
            self.conn, self.addr = self.listen(self.s, self.ip, self.port)
            threading.Thread(target=self.readData, args=(buffer,)).start()
        elif self.server_type == "connect":
            self.connect(self.s, self.ip, self.port)
            self.conn = self.s
            threading.Thread(target=self.readData, args=(buffer,)).start()

    def listen(self, s, ip, port):
        print('--listen called--')
        s.bind((ip, port))
        s.listen()
        conn, addr = s.accept()
        return conn, addr

    def connect(self, s, ip, port):
        print('--connect called--')
        s.connect((ip, port))

    def sendMessage(self, message):
        print('--send message called--')
        self.conn.sendall(message)

    def readData(self, buffer):
        print('--read data called--')
        while True:
            data = self.conn.recv(1024)
            if data:
                with self.mutex:
                    buffer += data

def bytes_to_int(bytes_obj):
    result = 0
    for b in bytes_obj:
        result = result * 256 + int(b)
    return result

key = bytes.fromhex('5a215b82583b90ae15fb03403893981311111111111111112222222222222222')

encryptor = AES(key)
socket_type = "connect"
callback_ip = "127.0.0.1"
callback_port = "1234"
buffer = bytearray()
testClient = TCPClient(socket_type, callback_ip, int(callback_port), buffer)
mutex = threading.Lock()
received = bytes()

while True:
    message = input()
    if message == "p":
        with mutex:
            received += bytes(buffer)
            buffer.clear()
        if received:
            # test if received contains a full message
            #print(received.hex())
            length = bytes_to_int(received[:2])
            #print(length)
            if len(received) < (length + 2):
                continue
            iv = received[2:18]
            ciphertext = received[18:(length+2)]
            plaintext = encryptor.decrypt_cbc(ciphertext,iv)
            #print(iv.hex())
            print(plaintext)
            received = received[(length+2):]
    elif message == "quit":
        exit
    else:
        plaintext = bytes(message, 'utf-8')
        while len(plaintext) > 65500:
            current_message = plaintext[:65000]
            plaintext = plaintext[65000:]
            iv = os.urandom(16)
            ciphertext = encryptor.encrypt_cbc(current_message,iv)
            length = format(len(iv) + len(ciphertext),'04x')
            length_header = bytes.fromhex(length)
            testClient.sendMessage(length_header+iv+ciphertext)

        iv = os.urandom(16)
        ciphertext = encryptor.encrypt_cbc(plaintext,iv)
        length = format(len(iv) + len(ciphertext),'04x')
        length_header = bytes.fromhex(length)
        testClient.sendMessage(length_header+iv+ciphertext)

