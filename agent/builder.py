import argparse
import os
import time

file_list = ['aes.py','basic_server.py']

def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-c', '--callback', action='store_true', help='agent will callback')
    group.add_argument('-l', '--listen', action='store_true', help='agent will listen')
    parser.add_argument('-p','--port',required=True, help='port to listen on or callback to')
    parser.add_argument('ip', help='ip to listen on or callback to')
    parser.add_argument('-key',help='specify aes key manually')
    args = parser.parse_args()
    print(args)

    key = os.urandom(16) if args.key == None else bytes.fromhex(args.key)
    current_time = time.time()
    filename = "pack_" + str(current_time)
    port = args.port
    ip = args.ip

    print("key:",key.hex())
    print('current_time:', current_time)
    print('filename:', filename)
    print('port:', port)
    print('ip:', ip)

    # build file_content dict with lines from all files
    file_content = {}
    for file in file_list:
        with open(file,'r') as f:
            file_content[file] = f.readlines()

    # build imports list
    imports_list = []
    for file in file_content:
        imports_list += [line for line in file_content[file] if line.startswith('import')]
        file_content[file] = [line for line in file_content[file] if not line.startswith('import')]
    imports_list = list(set(imports_list))

    import pprint; pprint.pprint(file_content)






    if args.listen:
        pass
        
    if args.callback:
        pass

if __name__ == "__main__":
    main()
